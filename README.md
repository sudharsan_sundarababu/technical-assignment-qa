# Technical QA Assignments

This repository demontrates :
- Functional api test automation.
- End to End Web App test sutomation.

## Software pre-requisites
- Latest Docker Desktop Community Edition

## Run tests
To run functional api tests, issue the following command
```
> # Build docker image
> docker build -t api-tests ./api-tests/
>
>
> # Run tests
> docker run  api-tests
```
_The above tests fails for a legit reason._

To run E2E web application tests, issue the following command
```
> # Build docker image
> docker build -t e2e-web-tests ./e2e-web-tests/
>
>
> # Run tests
> docker run e2e-web-tests
```