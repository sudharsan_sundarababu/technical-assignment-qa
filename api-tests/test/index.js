const { describe, it, before } = require('mocha');
const { assert } = require('chai');
const util = require('util');
const { uuid } = require('uuidv4');

const { apis, endpoint } = require('./lib/config');
const { get, post } = require('./lib/api');
const { random, isNumberWithinRange } = require('./lib/helper');

describe(`RESTful API endpoint: ${endpoint}`, async function () {
  this.timeout(0);
  let user;
  let posts = [];

  describe('GET /users', async function () {
    const { getUsers: { url } } = apis;
    let users;

    it('should return user list', async function () {
      try {
        const response = await get(url);
        assert.equal(response.status, 200, `Expected HTTP Response code to be 200OK, but received ${response.status}`);

        users = response.data;
        assert.typeOf(users, 'Array');
        assert.isAtLeast(users.length, 1, 'Expected atleast 1 users , but found no users');
      } catch (e) {
        assert.fail(e);
      }
    });

    it('should return email address of the user', async function () {
      try {
        user = users[random(users.length)];

        const { email } = user;
        assert.isNotEmpty(email);
        assert.typeOf(email, 'string');

        console.log(`\tEmail address of the user: ${email}`);
      } catch (e) {
        assert.fail(e);
      }
    });
  });

  describe('GET /posts', async function () {
    let { getPosts: { url } } = apis;

    it('should return posts per user', async function () {
      try {
        url = url.replace('=id', `=${user.id}`);
        const response = await get(url);
        assert.equal(response.status, 200, `Expected HTTP Response code to be 200OK, but received ${response.status}`);

        posts = response.data;
        this.posts = posts;
        assert.typeOf(posts, 'Array');
        assert.isAtLeast(posts.length, 1, `Expected atleast 1 post, but found no posts for the user with id: ${user.id} `);
      } catch (e) {
        assert.fail(e);
      }
    });

    it('post id should be in the range 1..100', async function () {
      try {
        posts.forEach((item) => {
          assert.isNumber(item.id, `Expected post id to be a number, but found it as ${typeof (item.id)}`);
          assert.equal(isNumberWithinRange(item.id), true, `Expected post id between 1..100, but found ${item.id}`);
        });
      } catch (e) {
        assert.fail(e);
      }
    });
  });

  describe('POST /posts', async function () {
    let userId;
    let postId;
    let newPost;
    let response;

    before(async function () {
      try {
        const { getUsers: { url } } = apis;
        response = await get(url);
        const users = response.data;
        const userObject = users[random(users.length)];
        userId = userObject.id;
        postId = random(100, 1);
      } catch (e) {
        userId = 1;
        postId = random(100, 1);
      }
    });


    it('should allow user to publish a new message post', async function () {
      const { postMessage: { url, headers, body } } = apis;
      newPost = Object.assign(body, {
        userId,
        id: postId,
        title: uuid().toString(),
        body: uuid().toString(),
      });

      try {
        response = await post(url, headers, newPost);
        assert.equal(response.status, 201, `Expected HTTP Response code to be 201, but received ${response.status}`);
      } catch (e) {
        assert.fail(e);
      }
    });

    it('should store same message that is created', async function () {
      try {
        assert.deepEqual(
          response.data,
          newPost,
          `Expected to see this new post ${util.inspect(newPost, true, null, true)}, but found ${util.inspect(response.data, true, null, true)}`
        );
      } catch (e) {
        assert.fail(e);
      }
    });
  });
});
