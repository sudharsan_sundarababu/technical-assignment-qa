const endpoint = 'https://jsonplaceholder.typicode.com';

const apis = {
  getUsers: {
    url: `${endpoint}/users`,
  },
  getPosts: {
    url: `${endpoint}/posts?userId=id`,
  },
  postMessage: {
    url: `${endpoint}/posts`,
    headers: {
      'content-type': 'application/json'
    },
    body: {
      userId: 1,
      id: 1,
      title: 'title',
      body: 'body',
    },
  },
};

module.exports = {
  apis,
  endpoint,
};
