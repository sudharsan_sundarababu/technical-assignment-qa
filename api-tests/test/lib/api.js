const axios = require('axios');

const get = async (url) => axios.get(url);

const post = async (url, headers, body) => axios({
  method: 'POST',
  url,
  headers,
  data: body,
});

module.exports = { get, post };
