const random = (max, min = 0) => Math.floor(Math.random() * (max - min) + min);

const isNumberWithinRange = (number, min = 1, max = 100) => {
  if (number >= min && number <= max) return true;
  return false;
};

module.exports = { random, isNumberWithinRange };
