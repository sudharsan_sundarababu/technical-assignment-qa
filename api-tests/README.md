# Functional API Tests

This repository validates RESTful APIs exposed from the endpoint `https://jsonplaceholder.typicode.com/`


## Development

The tests use `mocha` test framework, `chai` assertion library and `airbnb` code linting specifications.

To install the project, issue these command
```
> cd api-tests
> npm install
```

To verify if source code is linted successfully, issue the command
```
> npm run lint
```

## Run tests

To run tests in node way, issue the following command
```
> npm run start
```

To run the tests using docker, issue the following command
```
> # Build docker image
> docker build -t api-tests .
>
>
> # Run tests
> docker run  api-tests
```

## TODOs
- Implement unit tests for /lib/*.js