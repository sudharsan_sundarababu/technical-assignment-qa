const searchEngineUrl = 'https://google.co.uk';

const launchOptions = () => {
  const options = {
    headless: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
  };
  if (process.platform !== 'darwin') {
    options.executablePath = '/usr/bin/google-chrome-stable';
  }
  return options;
};

module.exports = {
  searchEngineUrl,
  launchOptions,
};
