const puppeteer = require('puppeteer');

const { launchOptions } = require('../lib/config');

class Browser {
  constructor() {
    /* eslint-disable no-unused-expressions */
    this.browser;
  }

  async browser() {
    this.browser = await puppeteer.launch(launchOptions());
    return this.browser;
  }

  async close() {
    await this.browser.close();
  }

}

module.exports = { Browser };
