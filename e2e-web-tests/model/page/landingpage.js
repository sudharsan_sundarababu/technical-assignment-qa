const { assert } = require('chai');

/* eslint-disable no-undef */
class LandingPage {
  constructor(page) {
    this.page = page;
    this.resultMarker = 'Searches related to';
  }

  async viewResults() {
    await this.page.waitForNavigation({ waitUntil: 'networkidle0' });
    const bodyHTML = await this.page.evaluate(() => document.body.innerHTML);
    assert.equal(bodyHTML.includes(this.resultMarker), true, 'No search results found');
  }

  async viewResultsFrom(custom) {
    const bodyHTML = await this.page.evaluate(() => document.body.innerHTML);
    assert.equal(bodyHTML.includes(custom), true, `No search results found from ${custom}`);
  }

}

module.exports = { LandingPage };
