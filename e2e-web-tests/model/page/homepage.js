const SEARCH_HOME_URL = 'https://www.google.co.uk';

class HomePage {
  constructor(page) {
    this.page = page;
  }

  async open() {
    await this.page.goto(SEARCH_HOME_URL);
  }

  async search(searchString) {
    await this.page.waitFor('input[name=q]');
    await this.page.type('input[name=q]', searchString);
    await this.page.keyboard.press('Enter');
  }

}

module.exports = { HomePage };
