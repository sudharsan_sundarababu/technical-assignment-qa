const {
  Given,
  When,
  Then,
} = require('cucumber');
const { setDefaultTimeout } = require('cucumber');

const { Browser } = require('../../model/Browser');
const { HomePage } = require('../../model/page/homepage');
const { LandingPage } = require('../../model/page/landingpage');

setDefaultTimeout(60 * 1000);

Given('I am a web user', async function () {
  this.browser = await (new Browser()).browser();
});

When('I search for {string}', async function (string) {
  this.page = await this.browser.newPage();
  this.homePage = new HomePage(this.page);
  await this.homePage.open();
  await this.homePage.search(string);
});

Then('I should see search results', async function () {
  this.landingPage = new LandingPage(this.homePage.page);
  await this.landingPage.viewResults();
});

Then('I should see result from {string}', async function (string) {
  await this.landingPage.viewResultsFrom(string);
  await this.browser.close();
});
