Feature: Cars search in Google

   As a google search user,
   I want to search for cars in London,
   so that I can view the search results

Scenario: Google search results from Gumtree
Given I am a web user
When I search for "Cars in London"
Then I should see search results
And I should see result from "gumtree.com"
