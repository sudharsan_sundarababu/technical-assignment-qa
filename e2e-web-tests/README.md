# E2E Web tests

This repository validates tests for validation 'Cars in London' web search journey.

## Development

The tests use `cucumber-js` test framework, `chai` assertion library and `airbnb` code linting specifications.

To install the project, issue these command
```
> cd e2e-web-tests
> npm install
```

To verify if source code is linted successfully, issue the command
```
> npm run lint
```

## Run tests

To run tht test with a headless browser in node way, issue the following command
```
> npm run start
```

To run tht test with a chromeful browser in node way, update the value JSON attribute `headless` from `true` to `false` in the file `./e2e-web-tests/lib/config.js`

To run the tests using docker, issue the following command
```
> # Build docker image
> docker build -t e2e-web-tests .
>
>
> # Run tests
> docker run e2e-web-tests
```
